(function($) {
    $(document).ready(function () {
        "use strict";
   
        $('.slider-center').slick({
            centerMode: true,
            infinite: true,
            centerPadding: '27%',
            slidesToShow: 1,
            speed: 500,
            variableWidth: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        centerMode: false
                    }
                }
            ]
        });

        $(window).load(function () {
            $('.navbar-nav > li > a').click(function (e) {
                //console.log("Clicked");
                $('.navbar-nav > li').removeClass('active');
                $(this).closest('li').addClass('active');
                e.preventDefault();
            });
        });
        /* ---------------------------------------------
         Scripts scroll
         --------------------------------------------- */
        $('.navbar-nav li > a').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top,
            }, 500, 'linear');
            event.preventDefault();
        });

        var mn = $(".main-nav")

        $(window).scroll(function() {
            if( $(this).scrollTop() > $(window).height() ) {
                mn.addClass("main-nav-scrolled");
            }
            else {
                mn.removeClass("main-nav-scrolled");
            }
        });
        // Scroll top
        $(document).on('click', '.scroll_top', function () {
            $('body,html').animate({scrollTop: 0}, 400);
            return false;
        });
    })
}(jQuery));